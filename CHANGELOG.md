# CHANGELOG for data aggregator

## 2024-05-07

- Rewritten Dockerfile. Changes invocation.
- Converted to Poetry with src-layout.
- Added unit tests.
- Added GitLab CI job for unit tests.
- Retrying on specific errors (S3/Minio).
- Removed dependence on Scrapy.
- Package is runnable as python module, i.e. `python -m aggregator <args>`
- Added pre-commit configuration.
- Updated README.
- Added LICENSE file.
- Added CHANGELOG (this file).
- Cleaned out unused files.
