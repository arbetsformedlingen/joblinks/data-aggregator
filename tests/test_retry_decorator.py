import pytest  # noqa: F401

from data_aggregator.utils import retry


def test_retry_decorator_calls_function():
    @retry(3, ZeroDivisionError)
    def test_fn():
        return "test_return_value"

    assert test_fn() == "test_return_value"


def test_retry_decorator_can_throw_error():
    @retry(3, ZeroDivisionError)
    def test_fn():
        raise ZeroDivisionError

    with pytest.raises(ZeroDivisionError):
        test_fn()


def test_retry_decorator_retries_times_times():
    count = 0

    @retry(3, ZeroDivisionError)
    def test_fn():
        # Bring in the variable count from the surrounding scope to easily count executions.
        nonlocal count
        count += 1

        raise ZeroDivisionError

    with pytest.raises(ZeroDivisionError):
        test_fn()

    assert count == 3


def test_retry_decorator_can_throw_error_not_added():
    count = 0

    @retry(3, IndexError)
    def test_fn():
        # Bring in the variable count from the surrounding scope to easily count executions.
        nonlocal count
        count += 1

        if count == 2:
            raise ZeroDivisionError
        else:
            raise IndexError

    with pytest.raises(ZeroDivisionError):
        test_fn()


def test_retry_decorator_accepts_multiple_exceptions():
    @retry(3, (IndexError, ZeroDivisionError))
    def test_fn():
        raise ZeroDivisionError

    with pytest.raises(ZeroDivisionError):
        test_fn()
