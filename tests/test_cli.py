import sys
import pytest  # noqa: F401
from unittest.mock import patch

from data_aggregator import cli


@patch("data_aggregator.data_aggregator.create_client")
@patch("data_aggregator.data_aggregator.process_spiders")
@patch.object(sys, "argv", ["data_aggregator", "spider_a", "spider_b"])
def test_process_spiders_basic(mock_process_spiders, mock_create_client):
    # Arrange
    mock_create_client.return_value = "test_client"

    # Act
    cli()

    # Assert
    mock_process_spiders.assert_called_once_with(
        ["spider_a", "spider_b"], "test_client"
    )

    mock_create_client.assert_called_once()


# @patch("sys.exit")
@patch("data_aggregator.data_aggregator.create_client")
@patch("data_aggregator.data_aggregator.process_spiders")
@patch.object(sys, "argv", ["data_aggregator", "spider_a", "spider_b"])
def test_process_spiders_no_results_exits_with_code_one(
    mock_process_spiders, mock_create_client
):
    # Arrange
    mock_create_client.return_value = "test_client"
    mock_process_spiders.return_value = 0

    # Act
    with pytest.raises(SystemExit) as error:
        cli()

    # Assert
    mock_process_spiders.assert_called_once_with(
        ["spider_a", "spider_b"], "test_client"
    )

    mock_create_client.assert_called_once()

    assert error.type == SystemExit
    assert error.value.code == 1


@patch.object(sys, "argv", ["data_aggregator"])
def test_process_spiders_no_spiders_exits_with_code_zero():
    # Arrange

    # Act
    with pytest.raises(SystemExit) as error:
        cli()

    # Assert
    assert error.type == SystemExit
    assert error.value.code == 0
