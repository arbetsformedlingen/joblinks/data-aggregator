import pytest  # noqa: F401
from unittest.mock import Mock, call, patch
import io

from data_aggregator import process_spiders


@patch("sys.stdout", new_callable=io.StringIO)  # noqa: F821
@patch("data_aggregator.data_aggregator.load_items_from_obj")
@patch("data_aggregator.data_aggregator.get_newest_obj_by_spider")
def test_process_spiders(
    mock_get_newest_obj_by_spider,
    mock_load_items_from_obj,
    mock_stdout,
):
    # Arrange
    spiders = ["spider_a", "spider_b"]
    mock_client = Mock()

    # Return values for get_newest_obj_by_spider
    newest_obj_values = [
        {
            "Key": "spider_a/2024-04-15.json",
            "Size": 399860,
        },
        {
            "Key": "spider_b/2024-04-15.json",
            "Size": 860399,
        },
    ]
    mock_get_newest_obj_by_spider.side_effect = (
        lambda client, spider: newest_obj_values.pop(0)
    )

    # Return values for load_items_from_obj
    items = [
        [
            {"identifier": "123"},
            {"identifier": "789"},
        ],
        [
            {"identifier": "456"},
            {"identifier": "789"},
            {"identifier": "789"},
        ],
    ]
    mock_load_items_from_obj.side_effect = lambda client, obj: items.pop(0)

    # Act
    count = process_spiders(spiders, mock_client)

    # Assert

    ## Should look for last file for each spider
    assert mock_get_newest_obj_by_spider.call_args_list == [
        call(mock_client, "spider_a"),
        call(mock_client, "spider_b"),
    ]

    ## Should load items from each file
    assert mock_load_items_from_obj.call_args_list == [
        call(mock_client, {"Key": "spider_a/2024-04-15.json", "Size": 399860}),
        call(mock_client, {"Key": "spider_b/2024-04-15.json", "Size": 860399}),
    ]

    assert count == 3

    # ## Should write to stdout
    assert (
        mock_stdout.getvalue()
        == '[\n{"identifier": "123", "scraper": "spider_a"},\n{"identifier": "789", "scraper": "spider_a"},\n{"identifier": "456", "scraper": "spider_b"}\n]\n'
    )


@patch("data_aggregator.data_aggregator.get_newest_obj_by_spider")
@patch("data_aggregator.data_aggregator.TemporaryFile")
def test_process_spiders_handles_missing_spider_file(
    MockTemporaryFile, mock_get_newest_obj_by_spider
):
    # Arrange
    mock_t = MockTemporaryFile.return_value.__enter__.return_value

    spiders = ["test_spider"]
    mock_client = Mock()

    # Return values for get_newest_obj_by_spider
    mock_get_newest_obj_by_spider.return_value = None

    # Act
    count = process_spiders(spiders, mock_client)

    # Assert
    ## TempFileContextManager should be initialized
    MockTemporaryFile.assert_called_once_with(mode="w+")

    ## Should look for last file for each spider
    assert mock_get_newest_obj_by_spider.call_args_list == [
        call(mock_client, "test_spider"),
    ]

    assert count == 0

    ## Should not write to file
    mock_t.write_to_file.assert_not_called()
    mock_t.write_line_break.assert_not_called()


@patch("data_aggregator.data_aggregator.get_newest_obj_by_spider")
@patch("data_aggregator.data_aggregator.TemporaryFile")
def test_process_spiders_handles_empty_spider_file(
    MockTemporaryFile, mock_get_newest_obj_by_spider
):
    # Arrange
    mock_t = MockTemporaryFile.return_value.__enter__.return_value

    spiders = ["test_spider"]
    mock_client = Mock()

    # Return values for get_newest_obj_by_spider
    mock_get_newest_obj_by_spider.return_value = {
        "Key": "test_spider/2024-04-15.json",
        "Size": 0,
    }

    # Act
    count = process_spiders(spiders, mock_client)

    # Assert
    ## TempFileContextManager should be initialized
    MockTemporaryFile.assert_called_once_with(mode="w+")

    ## Should look for last file for each spider
    assert mock_get_newest_obj_by_spider.call_args_list == [
        call(mock_client, "test_spider"),
    ]

    assert count == 0

    ## Should not write to file
    mock_t.write_to_file.assert_not_called()
    mock_t.write_line_break.assert_not_called()
