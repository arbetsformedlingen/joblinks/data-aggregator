import pytest  # noqa: F401
import datetime
from dateutil.tz import tzutc

from data_aggregator.data_aggregator import get_newest_obj_by_spider


def test_get_newest_obj_by_spider_return_none_when_zero_objects(mocker):
    # Arrange
    client_mock = mocker.Mock()
    list_objects_v2_mock = client_mock.list_objects_v2 = mocker.Mock()
    list_objects_v2_mock.return_value = {}

    # Act
    obj = get_newest_obj_by_spider(client_mock, "some.test.spider")

    list_objects_v2_mock.assert_called_once_with(
        Bucket="bucket-in-test", Prefix="some.test.spider"
    )

    # Assert
    assert obj is None


def test_get_newest_obj_by_spider_return_last_modified_object(mocker):
    # Arrange
    object_a = {
        "Key": "some.test.spider/2024-04-15.json",
        "LastModified": datetime.datetime(
            2024, 4, 15, 12, 56, 56, 547000, tzinfo=tzutc()
        ),
        "ETag": '"5d60c4ac73b15cfe76c2f8600113fde0"',
        "Size": 860399,
        "StorageClass": "STANDARD",
    }
    object_b = {
        "Key": "some.test.spider/2024-04-23.json",
        "LastModified": datetime.datetime(
            2024, 4, 23, 13, 53, 18, 658000, tzinfo=tzutc()
        ),
        "ETag": '"d41d8cd98f00b204e9800998ecf8427e"',
        "Size": 20340,
        "StorageClass": "STANDARD",
    }
    object_c = {
        "Key": "some.test.spider/2024-02-13.json",
        "LastModified": datetime.datetime(
            2024, 2, 13, 16, 16, 59, 857000, tzinfo=tzutc()
        ),
        "ETag": '"d41d8cd98f00b204e9800998ecf8427e"',
        "Size": 120345,
        "StorageClass": "STANDARD",
    }

    client_mock = mocker.Mock()
    list_objects_v2_mock = client_mock.list_objects_v2 = mocker.Mock()
    list_objects_v2_mock.return_value = {
        "Contents": [
            object_a,
            object_b,
            object_c,
        ]
    }

    # Act
    obj = get_newest_obj_by_spider(client_mock, "some.test.spider")

    # Assert
    assert obj is object_b

    list_objects_v2_mock.assert_called_once_with(
        Bucket="bucket-in-test", Prefix="some.test.spider"
    )


def test_get_newest_obj_by_spider_return_object_from_paginated_response(mocker):
    # Arrange
    object_a = {
        "Key": "some.test.spider/2024-04-15.json",
        "LastModified": datetime.datetime(
            2024, 4, 15, 12, 56, 56, 547000, tzinfo=tzutc()
        ),
        "ETag": '"5d60c4ac73b15cfe76c2f8600113fde0"',
        "Size": 860399,
        "StorageClass": "STANDARD",
    }
    object_b = {
        "Key": "some.test.spider/2024-04-23.json",
        "LastModified": datetime.datetime(
            2024, 4, 23, 13, 53, 18, 658000, tzinfo=tzutc()
        ),
        "ETag": '"d41d8cd98f00b204e9800998ecf8427e"',
        "Size": 20340,
        "StorageClass": "STANDARD",
    }
    object_c = {
        "Key": "some.test.spider/2024-02-13.json",
        "LastModified": datetime.datetime(
            2024, 2, 13, 16, 16, 59, 857000, tzinfo=tzutc()
        ),
        "ETag": '"d41d8cd98f00b204e9800998ecf8427e"',
        "Size": 120345,
        "StorageClass": "STANDARD",
    }

    client_mock = mocker.Mock()
    list_objects_v2_mock = client_mock.list_objects_v2 = mocker.Mock()
    list_objects_v2_mock.side_effect = [
        # First response
        {
            "Contents": [
                object_a,
            ],
            "NextContinuationToken": "continuation-token-in-test",
        },
        # Second response
        {
            "Contents": [
                object_b,
                object_c,
            ]
        },
    ]

    # Act
    obj = get_newest_obj_by_spider(client_mock, "some.test.spider")

    # Assert
    assert obj is object_b

    list_objects_v2_mock.assert_any_call(
        Bucket="bucket-in-test", Prefix="some.test.spider"
    )

    list_objects_v2_mock.assert_any_call(
        Bucket="bucket-in-test",
        Prefix="some.test.spider",
        ContinuationToken="continuation-token-in-test",
    )
