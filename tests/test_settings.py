import sys
import pytest  # noqa: F401

import os

from unittest.mock import patch


@pytest.fixture(autouse=True)
def remove_import_after_test():
    try:
        del sys.modules["data_aggregator.settings"]
    except KeyError:
        pass

    yield


@pytest.mark.noautofixture
@patch.dict(os.environ, values={}, clear=True)
def test_defaults_settings():
    from data_aggregator.settings import Settings

    assert Settings.AWS_ENDPOINT_URL == "define-it-in-env-var"
    assert Settings.AWS_DELIVER_BUCKET == "define-it-in-env-var"
    assert Settings.AWS_ACCESS_KEY_ID == "define-it-in-env-var"
    assert Settings.AWS_SECRET_ACCESS_KEY == "define-it-in-env-var"


@pytest.mark.noautofixture
@patch.dict(
    os.environ, values={"AWS_ENDPOINT_URL": "http://test.example.com:7654"}, clear=True
)
def test_setting_AWS_ENDPOINT_URL():
    from data_aggregator.settings import Settings

    assert Settings.AWS_ENDPOINT_URL == "http://test.example.com:7654"


@pytest.mark.noautofixture
@patch.dict(os.environ, values={"AWS_DELIVER_BUCKET": "some-bucket-name"}, clear=True)
def test_setting_AWS_DELIVER_BUCKET():
    from data_aggregator.settings import Settings

    assert Settings.AWS_DELIVER_BUCKET == "some-bucket-name"


@pytest.mark.noautofixture
@patch.dict(os.environ, values={"AWS_ACCESS_KEY_ID": "some-test-user"}, clear=True)
def test_setting_AWS_ACCESS_KEY_ID():
    from data_aggregator.settings import Settings

    assert Settings.AWS_ACCESS_KEY_ID == "some-test-user"


@pytest.mark.noautofixture
@patch.dict(
    os.environ, values={"AWS_SECRET_ACCESS_KEY": "some-test-password"}, clear=True
)
def test_setting_AWS_SECRET_ACCESS_KEY():
    from data_aggregator.settings import Settings

    assert Settings.AWS_SECRET_ACCESS_KEY == "some-test-password"
