import pytest  # noqa: F401

from data_aggregator.data_aggregator import create_client


def test_create_client(mocker):
    # Arrange
    MockConfig = mocker.patch(
        "data_aggregator.data_aggregator.Config",
        return_value="some_config_object",
    )
    boto3_client_mock = mocker.patch("boto3.client")

    # Act
    client = create_client()

    # Assert
    assert client

    MockConfig.assert_called_once_with(connect_timeout=2000, read_timeout=2000)

    boto3_client_mock.assert_called_once_with(
        "s3",
        aws_access_key_id="minio-user-in-test",
        aws_secret_access_key="minio-password-in-test",
        endpoint_url="http://endpoint-in-test:9000",
        config="some_config_object",
    )
