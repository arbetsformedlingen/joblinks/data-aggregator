import pytest

from unittest import mock


@pytest.fixture(autouse=True)
def settings(request):
    # Ignore if @pytest.mark.noautofixture is being used.
    if "noautofixture" in request.keywords:
        yield
        return

    class MockSettings(object):
        AWS_ENDPOINT_URL = "http://endpoint-in-test:9000"
        AWS_DELIVER_BUCKET = "bucket-in-test"
        AWS_ACCESS_KEY_ID = "minio-user-in-test"
        AWS_SECRET_ACCESS_KEY = "minio-password-in-test"

    with mock.patch("data_aggregator.data_aggregator.Settings", MockSettings):
        yield
