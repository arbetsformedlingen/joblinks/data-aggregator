import json
import pytest  # noqa: F401
from unittest.mock import Mock, patch

from data_aggregator.data_aggregator import load_items_from_obj


@patch("data_aggregator.data_aggregator.TemporaryFile")
def test_load_items_from_obj_returns_data(MockTemporaryFile):
    # Arrange
    mock_client = Mock()
    mock_s3_obj = {"Key": "some-test-key"}
    temp_file_data = [
        '{"test": "data"}',
        '{"foo": "bar"}',
        '{"@type": "JobPosting", "@context": "http://schema.org/", "identifier": "03171dfa-e741-48f3-abe2-c163cc3245ac", "datePosted": "2024-05-12", "validThrough": null, "title": "Grävmaskinist", "description": "Vi söker Grävmaskinist till vår återvinningsanläggning!", "url": "https://www.jobtechdev.se?gravmaskinist", "hiringOrganization": {"name": "Arbetsformedlingen", "logo": "https://arbetsformdelingen.se/logo.gif", "@type": "Organization", "@context": "http://schema.org/"}, "jobLocation": {"address": "Kiruna", "@type": "Place", "@context": "http://schema.org/"}, "scraper": "somescraper"}',
        '{"@type": "JobPosting", "@context": "http://schema.org/", "identifier": "0dc60b5a-c459-44a4-bb25-eaed31358a86", "datePosted": "2024-05-14", "Annonsbeskrivning här utan json-attribut.", "url": "https://www.jobtechdev.se?trasigjson"}',
    ]

    ctx_manager = MockTemporaryFile.return_value.__enter__.return_value

    ctx_manager.__iter__.return_value = temp_file_data

    # Act
    data = list(load_items_from_obj(mock_client, mock_s3_obj))

    # Assert
    expected = []
    for item in temp_file_data:
        try:
            expected.append(json.loads(item))
        except json.JSONDecodeError:
            pass

    assert data == expected

    mock_client.download_fileobj.assert_called_once_with(
        "bucket-in-test", "some-test-key", ctx_manager
    )

    ctx_manager.seek.assert_called_once_with(0)

    # Check that context manager exited.
    MockTemporaryFile.return_value.__exit__.assert_called_once()
