__doc__ = """
Data aggregator
---------------

Aggregate data from multiple spiders into a single location.
"""

__all__ = ["cli", "process_spiders"]

from .data_aggregator import cli, process_spiders
