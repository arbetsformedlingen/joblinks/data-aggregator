import os


class Settings(object):
    AWS_DELIVER_BUCKET = os.environ.get("AWS_DELIVER_BUCKET", "define-it-in-env-var")
    AWS_ACCESS_KEY_ID = os.environ.get("AWS_ACCESS_KEY_ID", "define-it-in-env-var")
    AWS_SECRET_ACCESS_KEY = os.environ.get(
        "AWS_SECRET_ACCESS_KEY", "define-it-in-env-var"
    )
    AWS_ENDPOINT_URL = os.environ.get("AWS_ENDPOINT_URL", "define-it-in-env-var")
