from functools import wraps
import logging


logger = logging.getLogger()


def retry(times, exceptions):
    """
    Retry decorator
    Will retry the decorated function `times` times if one of the exceptions
    in the list `exceptions` are thrown. Will rethrow the last the exception
    of the last attempt.
    """

    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            attempt = 0
            while attempt < times:
                try:
                    return f(*args, **kwargs)
                except exceptions as error:
                    attempt += 1
                    if attempt == times:
                        logger.warning(
                            f"Function {f} threw an error on it's final attempt. Reraising."
                        )
                        raise error
                    else:
                        logger.warning(
                            f"Function {f} threw the following error on attempt {attempt} of {times}.",
                            exc_info=error,
                        )

        return wrapper

    return decorator
