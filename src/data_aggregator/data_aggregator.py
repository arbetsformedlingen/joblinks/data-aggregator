import sys

import logging
import json
from json import JSONDecodeError
from tempfile import TemporaryFile

import boto3
from botocore.client import Config
from botocore.exceptions import ResponseStreamingError

from data_aggregator.utils import retry
from data_aggregator.settings import Settings

logging.basicConfig(level=logging.INFO, format="%(asctime)s:" + logging.BASIC_FORMAT)
logger = logging.getLogger()


def get_newest_obj_by_spider(client, spider):
    """
    Locate newest object for each spider
    It's an inefficient way as it need to load all objects
    Because boto3 does not support list objects in modified/created time order
    https://stackoverflow.com/questions/44574548/boto3-s3-sort-bucket-by-last-modified

    It returns objects in alphabet order
    which means we will get 2019-12-01.json and then 2020-01-01.json
    So we need to iterate all the pages

    In our case we won't have too much objects for each spider (runs weekly)
    And each page can return 1K objects as a maximum, so it's not too bad.
    """

    latest_obj = None
    kwargs = {"Bucket": Settings.AWS_DELIVER_BUCKET, "Prefix": spider}

    while True:
        resp = client.list_objects_v2(**kwargs)
        for obj in resp.get("Contents", []):
            if not latest_obj or (
                obj["LastModified"] > latest_obj["LastModified"] and obj["Size"] > 0
            ):
                latest_obj = obj
        try:
            kwargs["ContinuationToken"] = resp["NextContinuationToken"]
        except KeyError:
            break

    return latest_obj


@retry(3, ResponseStreamingError)
def load_items_from_obj(client, s3_obj):
    with TemporaryFile() as t:
        client.download_fileobj(Settings.AWS_DELIVER_BUCKET, s3_obj["Key"], t)

        t.seek(0)
        for line in t:
            try:
                yield json.loads(line)
            except JSONDecodeError:
                logger.error(f"Failed to parse json line: {line}")


def create_client():
    return boto3.client(
        "s3",
        aws_access_key_id=Settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=Settings.AWS_SECRET_ACCESS_KEY,
        endpoint_url=Settings.AWS_ENDPOINT_URL,
        config=Config(connect_timeout=2000, read_timeout=2000),
    )


def process_spiders(spiders, client):
    with TemporaryFile(
        mode="w+",
    ) as t:
        job_id_seen = set()
        total_duplicated_size = 0
        output_size = 0

        for spider in spiders:
            newest_obj = get_newest_obj_by_spider(client, spider)
            if not newest_obj:
                logging.warning(f"Unable to load scraped file for spider {spider}")
                continue
            if newest_obj.get("Size") == 0:
                logging.warning(f"Empty scraped file for spider {spider}")
                continue
            spider_job_id_seen = set()
            spider_duplicated_size, others_duplicated_size = 0, 0
            logging.info(f'processing items for {spider} from {newest_obj["Key"]}')

            for item in load_items_from_obj(client, newest_obj):
                job_id = item.get("identifier")
                if job_id:
                    if job_id in spider_job_id_seen:
                        spider_duplicated_size += 1
                        continue
                    spider_job_id_seen.add(job_id)
                    if job_id in job_id_seen:
                        others_duplicated_size += 1
                        continue
                    job_id_seen.add(job_id)
                item["scraper"] = spider
                t.write(json.dumps(item, ensure_ascii=False) + "\n")
                output_size += 1
            logging.info(
                f"processed for {spider}, "
                f"duplicated within spider {spider_duplicated_size}, "
                f"against other spider{others_duplicated_size}"
            )
            total_duplicated_size += others_duplicated_size

        logging.info(
            f"total output size {output_size}, {total_duplicated_size} duplications across spiders found"
        )

        t.seek(0)
        print("[", ",\n".join([line.rstrip() for line in t.readlines()]), "]", sep="\n")

        return output_size


def cli():
    spiders = sys.argv[1:]
    logging.info(f"Started. Spiders: {spiders} ")
    if not spiders:
        logging.info("No spiders")
        exit(0)  # exit if nothing to do
    client = create_client()

    output_size = process_spiders(spiders, client)

    if output_size == 0:
        logging.warning("Empty output")
        sys.exit(1)
