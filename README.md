# Jobtech Links Data Aggregator

## Project Overview

The data aggregator merges the files on minio that contains ads and writes the result on stdout.
The repo can be run either in docker container (i.e. as on remote environment) or in IDE (e.g. IntelliJ/PyCharm).
Running in docker container is good for making sure the docker config actually works and running in IDE is good
for debugging and testing while implementing.

## Set up environment

This project uses Poetry with `src` folder structure and installs as a package named `data_aggregator`. To install development environment a simple `poetry install` should be sufficient.

### pre-commit

Run `poetry run pre-commit install` to install the pre-commit hook in your repository.

## Tests

The data aggregator has a test setup that can be run with the following.

```shell
poetry run pytest --cov-report term-missing --cov src
```

`--cov src` scopes the coverage report to the `src` folder.
`--cov-report term-missing` lists all lines with missing coverage to the output.

## Running

The project can be run as a python module.

```sh
poetry run python -m data_aggregator <list of spiders>
```

### Using docker

Build the Docker image with:

```sh
docker build -t data_aggregator:latest .
```

Run the data aggregator in container with default spiders do:

```sh
docker run \
  -e AWS_ACCESS_KEY_ID=<access-key-id> \
  -e AWS_SECRET_ACCESS_KEY=<secret-access-key> \
  -e AWS_ENDPOINT_URL=<endpoint-url> \
  -e AWS_DELIVER_BUCKET=<deliver-bucket> \
  data_aggregator:latest
```

To run the data aggregator in container with custom list of spiders do:

```sh
docker run \
  -e AWS_ACCESS_KEY_ID=<access-key-id> \
  -e AWS_SECRET_ACCESS_KEY=<secret-access-key> \
  -e AWS_ENDPOINT_URL=<endpoint-url> \
  -e AWS_DELIVER_BUCKET=<deliver-bucket> \
  data_aggregator:latest spider_a spider_b spider_c
```

### Running data aggregator in IDE

- Install dependencies and set up a venv as when setting up scrapers.
- Set up a run configuration (Edit configurations) with the following options:
  - Working directory = `[base dir]scraping\venv`
  - Choose target to run = `script path` (instead of module name)
  - Script path = `[repo dir]\scripts\data_aggregator.py`
  - Example environment variables (see 'Environment variables' section for more):

    ```env
    AWS_ENDPOINT_URL=https://minio.arbetsformedlingen.se/;
    AWS_DELIVER_BUCKET=scrapinghub-dev;
    AWS_ACCESS_KEY_ID=scrapinghub-dev;
    AWS_SECRET_ACCESS_KEY=<secret-access-key>;
    AWS_CA_BUNDLE=[path\\to\\scraping]\\minio.pem;
    ```
