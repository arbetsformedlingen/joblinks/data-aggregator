#!/bin/sh

set -e

# Activate virtual environment
. /app/.venv/bin/activate

# Run passed command
exec "$@"
